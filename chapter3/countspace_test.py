import argparse
from soyspacing.countbase import CountSpace

parser = argparse.ArgumentParser(description='shinwc')

parser.add_argument('--str', required = True, help = 'good')

args = parser.parse_args()

model_fname = "/notebooks/embedding/data/processed/space-correct.model"
model = CountSpace()
model.load_model(model_fname, json_format=False)
print(model.correct(args.str))
