import argparse
from konlpy.tag import Okt, Komoran, Mecab, Hannanum, Kkma

parser = argparse.ArgumentParser(description='shinwc')

parser.add_argument('--str', required=True, help='good')
parser.add_argument('--tokenizer', required=True, help='good')
args = parser.parse_args()

def get_tokenizer(tokenizer_name):
    if tokenizer_name == "komoran":
        tokenizer = Komoran()
    elif tokenizer_name == "okt":
        tokenizer = Okt()
    elif tokenizer_name == "mecab":
        tokenizer = Mecab()
    elif tokenizer_name == "hannanum":
        tokenizer = Hannanum()
    elif tokenizer_name == "kkma":
        tokenizer = Kkma()
    else:
        tokenizer = Mecab()
    return tokenizer

# 코모란 사용 예시
tokenizer = get_tokenizer(args.tokenizer)
print(tokenizer.morphs(args.str))
print(tokenizer.pos(args.str))
