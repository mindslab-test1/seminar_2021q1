import argparse
from khaiii import KhaiiiApi


tokenizer = KhaiiiApi()

parser = argparse.ArgumentParser(description='shinwc')

parser.add_argument('--str', required=True, help='문장 입력')

args = parser.parse_args()
 
input_str = args.str

data = tokenizer.analyze(input_str)
tokens = []
for word in data:
    tokens.extend([str(m).split("/")[0] for m in word.morphs])

print(tokens)
