import argparse
from models.bert.tokenization import FullTokenizer

parser = argparse.ArgumentParser(description='shinwc')

parser.add_argument('--str', required = True, help='good')

args = parser.parse_args()

vocab_fname = "/notebooks/embedding/data/processed/bert.vocab"
tokenizer = FullTokenizer(vocab_file=vocab_fname, do_lower_case=False)

print(tokenizer.tokenize(args.str))
