import math
from soynlp.word import WordExtractor
from soynlp.tokenizer import LTokenizer
import argparse

parser = argparse.ArgumentParser(description='shinwc')

parser.add_argument('--str', required = True, help = 'good')

args = parser.parse_args()

model_fname = "/notebooks/embedding/data/processed/soyword.model"

word_extractor = WordExtractor(min_frequency=100, min_cohesion_forward=0.05, min_right_branching_entropy=0.0)
word_extractor.load(model_fname)
scores = word_extractor.word_scores()
scores = {key:(scores[key].cohesion_forward * math.exp(scores[key].right_branching_entropy)) for key in scores.keys()}
tokenizer = LTokenizer(scores = scores)
tokens = tokenizer.tokenize(args.str)
print(tokens)
