# 문서 작성 예제
## VIP (Virtual IP)

만약 LB+HA 구성을 물리적으로 분리된 서버에 하지 않고 LB의 대상이 되는 서버에 구성하는 경우 VIP(Virtual IP)를 설정할 필요가 있다. 
일단 외부 IP와 바인딩 가능하도록 OS설정 수정이 필요한데 아래와 같이 `/etc/sysctl.conf`를 변경한다. 

~~~~bash
echo 'net.ipv4.ip_nonlocal_bind=1' >> /etc/sysctl.conf
sysctl -p
~~~~
        

## keepalived 실행 환경
keepalived를 설치하면 기본적(설치할 때 path를 지정하지 않으면)으로 `/usr/local/sbin`에 binary 파일을 복사하고 아래의 두 경로에 설정 파일을 복사해 놓는다. 


 1. /usr/local/etc/keepalived/keepalived.conf: keepalived 기본설정 파일
 2. /usr/local/etc/sysconfig/keepalived: syslog 관련 설정 파일
 3. /usr/local/etc/sysconfig/keepalived 내용:
 
     * 변경 전 (설치되면 기본값)
            
            KEEPALIVED_OPTION="-D"
            
     * 변경 후: 옵션 `-P`는 VRRP(Virtual Router Redundancy Protocol) 서브시스템만 사용하겠다는 의미. 보통 keepalived와 리눅스 LVS(Linux Virtual Server)를 이용해서 LB를 구현하는 것도 가능한데 여기서는 LB는 HAProxy로 구현하고 keepalived의 VRRP 기능만 이용해서 HA 구성을 하기 때문에 필요한 옵션. 
            
            KEEPALIVED_OPTION="-D -P -f /usr/local/etc/keepalived/keepalived.conf"

Code Example with h1 header
===
Code Example with h2 header
---
Here's a code sample:

    # Let me re-iterate ...
    for i in 1 .. 10 { do-something(i) }

As you probably guessed, indented 4 spaces. 

Or use fenced code (with `markdown` v2.4):

~~~~sh
# Let me re-iterate ...
for i in 1 .. 10 { do-something(i) }
~~~~

If you have `minted` loaded in your project you get syntax-highlighted code:

~~~~python
    print("Hello World");
~~~~

~~~~php
<?php
    print("Hello World");
?>
~~~~

~~~js
function DeepThought(task) {
    this.task = task;
}
DeepThought.prototype.answer = function () {
    switch (this.task) {
        case "life, universe, and everything": return 42;
        default: throw "not implemented";
    }
}
~~~

~~~c++
TtsMediaResponse resp;

while (stream->Read(&resp)) {
    std::string bytes = resp.mediadata();
    std::cout << "[RX] TTS data: " << bytes.size() << std::endl;
    fwrite(bytes.c_str(), 1, bytes.size(), fp);
}
std::cout << "stream read end..." << std::endl;
~~~
